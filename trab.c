// Declaração das bibliotecas
#include <arpa/inet.h>
#include <assert.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// Declaração das constantes
#define PORT 80
#define FLAG 0
#define MAX 1024

// dada uma string base,
char *replaceWord(const char *s, const char *oldW, const char *newW) {
  char *result;
  int i, cnt = 0;
  int newWlen = strlen(newW);
  int oldWlen = strlen(oldW);

  // conta quantas vezes a palavra aparece na palavra antiga
  for (i = 0; s[i] != '\0'; i++) {
    if (strstr(&s[i], oldW) == &s[i]) {
      cnt++;

      i += oldWlen - 1;
    }
  }

  // novo tamanho para a string nova
  result = (char *)malloc(i + cnt * (newWlen - oldWlen) + 1);

  i = 0;
  while (*s) {
    if (strstr(s, oldW) == s) {
      strcpy(&result[i], newW);
      i += newWlen;
      s += oldWlen;
    } else
      result[i++] = *s++;
  }

  result[i] = '\0';
  return result;
}

// Retorna o status da conexão com sua respectiva explicação
int checkStatus(int status) {
  if (status < 200) {
    printf("Status: %d - A requisição foi bem sucedida mas não foi possível "
           "recuperar nenhuma informação\n",
           status);
  }
  if (status >= 200 && status < 300) {
    printf("Status: %d - Sucesso\n", status);
    return 1;
  }
  if (status >= 300 && status < 400) {
    printf("Status: %d - O recurso solicitado não está mais disponível neste "
           "domínio, verifique e tente novamente\n",
           status);
  }
  if (status >= 400 && status < 500) {
    if (status == 404) {
      printf("Status: %d - Página não encontrada, verifique a url informada e "
             "tente novamente\n",
             status);
      return 0;
    }
    printf("Status: %d - Não foi possível processar sua requisição, verifique "
           "se o domínio informado está disponível e tente novamente\n",
           status);
  }
  if (status >= 500) {
    printf("Status: %d - O servidor não conseguiu processar sua requisição, "
           "por favor, tente novamente mais tarde\n",
           status);
  }
  return 0;
}

int main(int argc, char *argv[]) {
  // Declaração das variaveis e structs
  char *url_input;
  char *ip_url_input;
  char *url_path;
  char *url_path_new;
  char http_string[MAX];
  int sockfd;
  int result;
  int count;
  int tam_path;
  int tam_input;
  struct addrinfo hints, *res;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  struct in_addr **addr_list;
  struct in_addr addr;
  int n_bytes, recv_length;
  char *body_init, test_char;
  int body_record = 0;
  FILE *file;

  url_path = (char *)malloc(512 * sizeof(char));
  url_path_new = (char *)malloc(512 * sizeof(char));
  // http_string = (char *)malloc(1024 * sizeof(char));
  //   Variavel host recebe o valor do argumento passado por CLI
  url_input = argv[1];

  // ######### - Tratamento necessário para a URL de entrada - #########
  // 1-Remoção do HTTP.
  // Pelo que foi verificado, a função gethostbyname() só recebe o parametro de
  // entrada sem o HTTP.
  if (strstr(url_input, "http://")) {
    url_input = replaceWord(url_input, "http://", "");
  }

  // Copia o valor da url_input para url_path (será utilizado mais a frente).
  // Isso é feito nesse momento devido ao fato de que precisamos da URL completa
  // para retirarmos o caminho.
  strcpy(url_path, url_input);

  // 2-Raiz da URL.
  // Para a função gethostbyname() funcionar corretamente é necessário informar
  // somente a raiz da URL sem os caminhos extras o qual ela possui.
  for (int i = 0; i < strlen(url_input); i++) {
    if (url_input[i] == '/') {
      url_input[i] = '\0';
      break;
    }
  }

  // 3-Caminho da URL.
  // Nesse momento vamos retirar a raiz da URL e obter o caminho da mesma.
  tam_path = strlen(url_path);
  tam_input = strlen(url_input);
  count = 0;

  if ((tam_path - tam_input) == 0) {
    url_path_new[count] = '/';
  } else if ((tam_path - tam_input) > 0) {
    for (int i = 0; i < strlen(url_path); i++) {
      if (url_path[i] == '/') {
        for (int j = i; j < strlen(url_path); j++) {
          url_path_new[count] = url_path[j];
          count++;
        }
        break;
      }
    }
  }

  // ######### - Pega informações sobre a URL informada pelo usuário - #########
  // 1-Pega o endereço IP da URL.
  server = gethostbyname(url_input);
  ip_url_input = inet_ntoa(*(struct in_addr *)server->h_addr);

  // Tratamento de erro, caso o retorno seja NULL a URL informada não existe
  if (server == NULL) {
    printf("Verifique a URL informada, algo está incorreto.\n");
    return 0;
  }

  // ######### - Manipulação do socket - #########
  // 1-Cria o socket.
  // Parametros da função socket(domain = AF_INET, type = SOCK_STREAM, protocol
  // = 0) domain = tipo de protocolo utilizado (PF_INET -> IPV4, PF_INET6 ->
  // IPV6, entre outras por ai) type = tipo de soquete utilizado (SOCK_STREAM ->
  // TCP, SOCK_DGRAM -> UDP, SOCK_RAW -> ICMP) protocol = determinar o real
  // protocolo utilizado (por padrão é 0) fonte:
  // https://www.sciencedirect.com/topics/computer-science/socket-function#:~:text=The%20socket%20function%20is%20used,first%20argument%20is%20always%20AF_INET.
  sockfd = socket(AF_INET, SOCK_STREAM, FLAG);

  if (sockfd == -1) {
    printf("\nErro ao criar o socket\n");
    return 0;
  } else {
    printf("Conexão efetuada com sucesso\n");
  }

  // 2-Conexão do socket.
  // Estrutura o qual deve ser definida antes de realizar o uso da função
  // connect() Observações sobre cada parametro: sin_port = Este elemento contém
  // o número da porta a ser conectada. O valor deve ser convertido em ordem de
  // bytes de rede usando a função ntohs. sin_addr = Este elemento simplesmente
  // contém o endereço de Internet do host ao qual estamos tentando nos
  // conectar. Normalmente, a função inet_addr é usada para converter o endereço
  // IP ASCII em dados binários reais. sin_family = Este elemento contém a
  // família de endereços, que em quase todos os casos é definida como AF_INET.

  memset(&serv_addr, 0, sizeof(serv_addr));

  /* Utiliza a função inet_addr para converter o endereço IP
  em dados binarios reais */
  serv_addr.sin_addr.s_addr =
      inet_addr(inet_ntoa(*(struct in_addr *)*(server->h_addr_list)));

  serv_addr.sin_family = AF_INET; // define AF_INET como o tipo de familia

  /* usa a função htons() para converter o valor 80 da
  porta em ordem de bytes de rede */
  serv_addr.sin_port = htons(80);

  /* Faz a conexão com a função connect() passando os parametros de cima */
  result = connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

  // ######### - Manipulando as requisições - #########
  // 1-Concatenando os parametros da requisição.
  // mudar o nome da variavel tt, ela recebe o retorno da função send()

  // a função sprintf formata a string de requisição com os parâmetros de url e
  // host
  sprintf(http_string, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", url_path_new,
          url_input);

  // send() realiza a requisição
  send(sockfd, http_string, strlen(http_string), 0);

  int rr, status, result_status;

  // recv faz a chamada para verificarmos o status da requisição
  rr = recv(sockfd, http_string, 12, 0);

  // pega o status da requisição e atribui para a variavel status
  sscanf(http_string, "%*s %d", &status);

  // a função checkstatus é invocada para imprimir o status da requisição
  // caso retorne algo diferente de 1, o programa, é encerrado
  result_status = checkStatus(status);

  if (!result_status) {
    return 0;
  }

  // abro o arquivo index.html em modo de escrita
  file = fopen("index.html", "w");

  // limpo a memória da variável http_string
  memset(&http_string, '\0', sizeof(http_string));

  // enquanto estiver recebendo dados do destino, o laço continuará sendo
  // executado
  while ((recv_length = recv(sockfd, http_string, sizeof(http_string), 0))) {

    // buscamos o fim do html para efetuarmos a parada do laço devido ao fato da
    // função recv não retornava o valor de parada menor ou igual a 0, conforme
    // a documentação
    // https://pubs.opengroup.org/onlinepubs/9699919799/functions/recv.html
    if (test_char = strstr(http_string, "</html>") != NULL) {

      // é feito um pós-processamento para remover caracteres indesejados após o
      // fim do fechamento da tag html
      for (int i = strlen(http_string); http_string[i] != '>'; i--) {
        if (http_string[i - 1] != 'l' && http_string[i - 2] != 'm' &&
            http_string[i - 3] != 't' && http_string[i - 4] != 'h') {
          http_string[i] = '\0';
        }
      }

      // o restante da última parte do html é então salvo no arquivo e em
      // seguida ocorre a parada do laço
      fprintf(file, "%s", http_string);
      break;
    }

    // validação inicial para não adicionarmos os headers da requisição, no html
    if (body_record == 0) {

      // a sequencia de caracteres \r\n\r\n identifica o fim dos headers
      // em seguida, pulamos para o caracter '<' para não adicionarmos linhas a
      // mais no arquivo html
      body_init = strstr(http_string, "\r\n\r\n");
      body_init = strstr(body_init, "<");

      // grava-se o inicio do body no arquivo
      if (body_init != NULL) {
        fputs(body_init, file);
      }
      body_record++; // incremento da flag para não executar novamente

      // memset para limparmos a variável armazenadora do payload da requisição
      memset(&http_string, '\0', sizeof(http_string));

      /* continue para não executarmos o código abaixo e
      pularmos para a próxima iteração, buscando mais dados */
      continue;
    }
    /* caracter '\0' é usado para demarcamos o
    fim da carga útil que desejamos gravar */
    http_string[recv_length] = '\0';
    fprintf(file, "%s", http_string);
    memset(&http_string, '\0', sizeof(http_string));
  };

  fclose(file); // fechamento do arquivo

  return 0;
}